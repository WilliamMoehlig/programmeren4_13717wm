<?php
namespace williamscloud\NoticeBoard;

class Notice
{
    public $text;
    public $owner;
    public $title;
    public $publishedOn;
    public $modifiedOn;
}
class NoticeBoard extends Notice
{
    public $notices = array();
    
    public function paste()
    {
        $temp = new Notice(); 
        $temp->title = $this->title;
        $temp->owner = $this->owner;
        $temp->text = $this->text;
        $this->notices[] = $temp;
    }
}

if (isset($_POST['txtText'])) {
    $text = $_POST['txtText'];
}
if (isset($_POST['txtTitel'])) {
    $titel = $_POST['txtTitel'];
}
if (isset($_POST['txtOwner'])) {
    $owner = $_POST["txtOwner"];
}

$nb = new NoticeBoard();

$nb->text ='Mijn tweede notitie';
$nb->title = 'Notitie 2';
$nb->owner = 'William Moehlig';
$nb->paste();

foreach ($_POST as $value) {
    
    $nb->text = $value[0];
    $nb->title = $value[1];
    $nb->owner = $value[2];
    $nb->paste();
    
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met klassen in PHP</title>
</head>
<body>
    <?php
    foreach ($nb->notices as $value) {
        ?>
        <h2><?php echo $value->title; ?></h2>
        <p><?php echo $value->text; ?></p>
        <p><small><?php echo $value->owner; ?></small></p>
        <?php
    }
    ?>
    <form method="post" action="klassen.php">
    <label for="txtText">Message</label>
    <input type="text" name="text" id="text"/></br>
    <label for="txtTitel">Titel</label>
    <input type="text" name="titel" id="titel"/></br>
    <label for="txtOwner">Eigenaar</label>
    <input type="text" name="owner" id="owner"/></br>
    <button>Verzend</button>
    </form>
</body>
</html>