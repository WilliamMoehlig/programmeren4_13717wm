<?php
namespace williammoehlig;
    require_once("noticeboard.php");
    require_once("notice.php");
    
    $noticeboard;
    $notice = null;
    $errorsFound = false;
    $titleErr = "";
    $textErr = "";
    $ownerErr = "";
    $cookieName = "persistObj";
    $cookieValue = null;
    
    if (isset($_COOKIE[$cookieName])) {
        $cookieName = "persistObj";
        $cookieValue = unserialize($_COOKIE[$cookieName]);
        $noticeboard = $cookieValue;
    }
    else 
    {
        $noticeboard = new \williammoehlig\NoticeBoard();
    }
    
    if (isset($_POST['title']) && isset($_POST['text']) && isset($_POST['owner']))
    {
        $testRegEx =  "/^[a-zA-Z0-9_éç.,: ]*$/";
        
        $title = $_POST['title'];
        if (empty($title)) 
        {
            $titleErr = 'Notice must have a title';
            $errorsFound = true;
        }
        else if (!preg_match($testRegEx, $title)) 
        {
            $productError = 'Notice can only be alphanumerical (including é, ç)';
            $errorsFound = true;
        }
        
        $text = $_POST['text'];
        if (empty($text)) 
        {
            $textErr = "Text can't be empty";
            $errorsFound = true;
        }
        else if (!preg_match($testRegEx,$text)) 
        {
            $textErr = "Text can only be alphanumerical";
            $errorsFound = true;
        }
        
        $owner = $_POST['owner'];
        if (empty($owner)) {
            $ownerErr = "Owner can't be empty";
            $errorsFound = true;
        } else if (!preg_match($testRegEx, $owner)) 
        {
            $ownerErr = "Owner can only be alphanumerical";
            $errorsFound = true;
        }
        
        if ($errorsFound == null) {
            $notice = new \williammoehlig\Notice();
            $notice->text = $text;
            $notice->title = $title;
            $notice->owner = $owner;
            $notice->publishedOn = date("d/m/Y, G:i:s");
            $noticeboard->addNotice($notice);
            $cookieValue = $noticeboard;
            setcookie($cookieName, serialize($cookieValue), time()+3600*24/7);
            $_COOKIE[$cookieName] = serialize($cookieValue);
            
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Noticeboard</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Noticeboard</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                    </ul>
                    <form class="navbar-form navbar-right">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                </div>
            </div>
        </nav>
        
        <div class="page-header" style="margin-top: 0;">
            <h1>Noticeboard <small>Get oriented with php</small></h1>
        </div>
        
        <?php
             if ($errorsFound) {
        ?>
            <div class="alert alert-danger" role="alert" style="width=100%;">
                <strong>Notice not added</strong>
                <ul>
                    <?php
                        if (!empty($textErr)) {
                            echo "<li>$textErr</li>";
                        }
                        if (!empty($titleErr)) {
                            echo "<li>$titleErr</li>";
                        }
                        if (!empty($ownerErr)) {
                            echo "<li>$ownerErr</li>";
                        }
                    ?>
                </ul>
            </div>
        <?php
        }
        ?>
        
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#addNotice">Create notice</button>

        <!-- Modal -->
        <div id="addNotice" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create new notice</h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" class="form-control" id="title" name="title" pattern="^[a-zA-Z0-9_éç: ]*$" required/>
                            </div>
                            <div class="form-group">
                                <label for="owner">Owner:</label>
                                <input type="text" class="form-control" id="owner" name="owner" pattern="^[a-zA-Z0-9_éç: ]*$" required/>
                            </div>
                            <div class="form-group">
                                <label for="text">Text:</label>
                                <textarea class="form-control" rows="5" id="text" name="text" style="resize: none;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Save</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        
        <!-- end of modal -->
        <div class="noticeboardWrapper row">
            <?php
            foreach ($noticeboard->getAllNotices() as $notice) {
            ?>
                <div class="col-md-3">
                <article class="panel panel-default">
                    <header class="panel-heading">
                        <?php echo $notice->title; ?>
                        <small>by <?php echo $notice->owner; ?></small>
                        <input type="hidden" name="id" value="<?php echo $notice->id; ?>"/>
                        <button type="button" name="btnDelete" id="btnDelete" class="button btn-danger" style="float: right;">Delete</button>
                    </header>
                    <div class="panel-body">
                        <p><?php echo $notice->text; ?></p>
                    </div>
                    <footer class="panel-footer">
                        <small>Created: <?php echo $notice->publishedOn; ?> - Last modified: <?php echo $notice->modified; ?></small>
                    </footer>
                </article>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</body>
</html>