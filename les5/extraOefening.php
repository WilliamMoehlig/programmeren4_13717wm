<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Extra oefening PHP</title>
</head>
<body>
    <h1>Formulier Extra oefening</h1>
    <p><span class="error">* verplicht veld</span></p>
    <form method="post">
       <fieldset>
           <ol>
               <li>
                   <label for="name">Naam</label>
                   <input type="text" name="name" autofocus required>
                   <span class="error">*</span>
               </li>
               <li>
                    <label for="email">E-mail</label>
                    <input type="email" name="email" required oninvalid="this.setCustomValidity('Username cannot be empty.')" onchange="this.setCustomValidity('')" placeholder="user@provider.name">
                    <span class="error">*</span>
                </li>
                <li>
                    <label for="website">Website</label>
                    <input type="url" name="website" placeholder="www.provider.name">
                </li>
                <li>
                    <textarea name="comment" rows="5" cols="40"></textarea>
                    <label for="comment">Commentaar</label>
                </li>
                <li>
                    <span>Hoe ben je op onze site terechtgekomen?</span>
                    <ol>
                        <li>
                            <input name="refer-random" type="checkbox" id="refer_random" value="random" />
                            <label for="refer-random">Toevallig</label>
                        </li>
                        <li>
                            <input name="refer-friend" type="checkbox" id="refer-friend" value="friend" />
                            <label for="refer-friend">Vriend</label>
                        </li>
                        <li>
                            <input name="refer-search-engine" type="checkbox" id="refer-search-engine" value="search-engine" />
                            <label for="refer-search-engine">Zoekmachine</label>
                        </li>
                    </ol>
                </li>
                <li>
                    <label for="rating">Wat vind je van deze site?</label>
                    <ol>
                        <li>
                            <input type="radio" name="rating" id="rate-5" value="5" />
                            <label for="rate-5">Schitterend</label>
                        </li>
                        <li>
                            <input type="radio" name="rating" id="rate-4" value="4">
                            <label for="rate-4">Goed</label>
                        </li>
                        <li>
                            <input type="radio" name="rating" id="rate-3" value="3">
                            <label for="rate-4">Mmmhh</label>
                        </li>
                        <li>
                            <input type="radio" name="rating" id="rate-2" value="2">
                            <label for="rate-4">Slecht</label>
                        </li>
                        <li>
                            <input type="radio" name="rating" id="rate-1" value="1">
                            <label for="rate-4">Heel slecht</label>
                        </li>
                    </ol>
                </li>
                <li>
                    <label for="country">Land</label>
                    <select name=country>
                        <option value="BE">België</option>
                        <option value="DE">Duitsland</option>
                        <option value="FR">Frankrijk</option>
                        <option value="LU">Luxemburg</option>
                        <option value="NL">Nederland</option>
                        <option value="UK">Verenigd Koninkrijk</option>
                    </select>
                </li>
           </ol>
       </fieldset>
       <div class="command-bar">
            <button type="submit" name="uc" value="User-Insert">Insert</button>
        </div>
    </form>
</body>
</html>