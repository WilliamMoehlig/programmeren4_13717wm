<?php
function checkNum($number)
{
    if (!filter_var($number, FILTER_VALIDATE_INT)) {
        throw new \Exception("De waarde is geen integer");
    }
}

try 
{
    checkNum('6');
} 
catch (\Exception $exception) 
{
    echo $exception->getMessage();
}