<?php
$productError = "";
$priceError = "";

if(isset($_POST['product']))
{
    $productName = $_POST['product'];
    if(empty($productName))
    {
        $productError = "product moet een naam hebben!";
    }
}

if(isset($_POST['prijs']))
{
    $price = $_POST['prijs'];
    if(empty($price))
    {
        $priceError = "product moet een prijs hebben!";
       
    }
    else
    {
        $pricePattern = "/^[0-9]+(\.[0-9]{1,2})?$/";
        if(!preg_match($pricePattern, $price))
        {
            $priceError = "Typ een geldig eurogetal in! bv 12.03";
        } 
    }
}

if(isset($_POST['gender']))
{
    $gender = $_POST['gender'];
    switch ($gender) {
        case 0 :
            $genderText = 'man';
            break;
        case 1 :
            $genderText = 'vrouw';
            break;        
        case 2 :
            $genderText = 'anders';
            break;
        default :
            $genderText = 'onbekend';
    }
}

if(isset($_POST['category']))
{
    $category = $_POST['category'];
    switch ($category) {
        case 0 :
            $categoryText = 'Huishoudgerief';
            break;
        case 1 :
            $categoryText = 'Electronica';
            break;        
        case 2 :
            $categoryText = 'Snoep';
            break;
        case 3 :
            $categoryText = 'Boeken';
        default :
            $categoryText = "Niets geselecteerd";
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulieer validatie</title>
</head>
<body>
    <p>Ebay</p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="product">Product</label>
        <input type="text" name="product"/ required></br>
        <span><?php echo $productError;?></span>
        <label for="prijs">Prijs</label>
        <input type="text" name="prijs"/ required title="Cijfers achter de komma moeten getypt worden met een punt" pattern="^[0-9]+(\.[0-9]{1,2})?$"></br>
        <span><?php echo $priceError;?></span>
        <label>Geslacht:</label>
        <label for="gender">Man</label>
        <input type="radio" id="man" name="gender" value="0">
        <label for="gender">Vrouw</label>
        <input type="radio" id="woman" name="gender" value="1">
        <label for="category">Categorie</label>
        <div>
        <select name="category" id="category">
                <option value="0">Huishoudgerief</option>
                <option value="1">Electronica</option>
                <option value="2">Snoep</option>
                <option value="3">Boeken</option>
        </select>
        </div>
        
        <button type="submit">Verzend</button>
    </form>
    <div>
            <p><?php echo "Je bent een {$genderText}";?></p>
            <p><?php echo "Je bent een {$categoryText}";?></p>
            
    </div>
</body>
</html>