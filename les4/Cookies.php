<?php
if (isset($_POST["assortiment"])) 
{
    if (isset($_COOKIE['cart'])) {
        $cart = unserialize($_COOKIE['cart']);
    }
    $cart[] = $_POST["assortiment"];
    setcookie('cart',serialize($cart), time()+(86400*7));
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>William's winkel</title>
</head>
<body>
  <form method="post" action="Cookies.php">
   <h1>William's winkel</h1>
   <label for="assortiment">Assortiment:</label>
   <select name="assortiment" id="assortiment">
      <option value="0">artikel 1</option>
      <option value="1">artikel 2</option> 
      <option value="2">artikel 3</option> 
      <option value="3">artikel 4</option> 
      <option value="4">artikel 5</option> 
      <option value="5">artikel 6</option> 
      <option value="6">artikel 7</option> 
      <option value="7">artikel 8</option> 
      <option value="8">artikel 9</option> 
      <option value="9">artikel 10</option> 
   </select>
   <button type=submit>Verzend</button>
  </form>
  
   <div>
        <?php
        if(isset($_COOKIE['cart'])) {
            $cart = unserialize($_COOKIE['cart']);
            foreach ($cart as $value) {
                echo $value.'<br />';
            }
        }else{
            echo 'Je cart is leeg.';
        }
        ?>
    </div>
</body>
</html>