<?php
session_set_cookie_params(86400 * 7);
session_start();
if (isset($_POST['assortiment'])) {
    if (isset($_SESSION['cart'])) {
        $cart = unserialize($_SESSION['cart']);
    }
    
    $cart[] = $_POST['assortiment'];
    
    $_SESSION['cart'] = serialize($cart);
}

if (isset($_GET['remove'])) {
    if (isset($_SESSION['cart'])) {
        $cart = unserialize($_SESSION['cart']);
    }
    unset($cart[$_GET['remove']]);
    if (empty($cart)) {
        session_unset();
    }
    else {
        $_SESSION['cart'] = serialize($cart);
    }
    
}
if (isset($_GET['empty'])) {
    session_unset();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>William's winkel</title>
</head>
<body>
  <form method="post" action="Sessions.php">
   <h1>William's winkel</h1>
   <label for="assortiment">Assortiment:</label>
   <select name="assortiment" id="assortiment">
      <option value="0">artikel 1</option>
      <option value="1">artikel 2</option> 
      <option value="2">artikel 3</option> 
      <option value="3">artikel 4</option> 
      <option value="4">artikel 5</option> 
      <option value="5">artikel 6</option> 
      <option value="6">artikel 7</option> 
      <option value="7">artikel 8</option> 
      <option value="8">artikel 9</option> 
      <option value="9">artikel 10</option> 
   </select>
   <button type=submit>Bestellen</button>
  </form>
  
  <div>
    <?php
        if (isset($_SESSION['cart'])) {
            $cart = unserialize($_SESSION['cart']);
            foreach ($cart as $key => $value) {
                echo $value.' - ';
                echo '<a href="Sessions.php?remove='.$key.'">Verwijder</a><br/>';
            }
            echo '<a href="Sessions.php?empty=all">Verwijder alles</a>';
        }
        else {
            echo 'Je cart is leeg.';
        }
    ?>
 </div>
</body>
</html>