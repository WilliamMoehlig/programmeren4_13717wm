<?php
$file = file_get_contents('Postcodes.csv');
$array = explode(PHP_EOL, $file);

foreach ($array as $key => $value) {
    list($postcodes[$key]['postcode'], $postcodes[$key]['plaats'],$postcodes[$key]['code postal'],$postcodes[$key]['ville']) = explode('|', $value);
}
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <table>
        <tr>
            <th>Postcode</th>
            <th>Plaats</th>
            <th>Provincie</th>
            <th>Ville</th>
        </tr>
        <?php
        foreach ($postcodes as $postcode) {
            ?>
            <tr>
                <td><?php echo $postcode['postcode']; ?></td>
                <td><?php echo $postcode['plaats']; ?></td>
                <td><?php echo $postcode['code postal']; ?></td>
                <td><?php echo $postcode['ville']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</body>
</html>