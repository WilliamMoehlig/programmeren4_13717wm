<?php
$geenVoornaam = "";
$geenFamilienaam = "";
$error = "false";
if(isset($_POST['voornaam']))
{
    $voorNaam = $_POST['voornaam'];
    if(empty($voorNaam))
    {
        $geenVoornaam = "Er moet een voornaam ingevuld worden!";
        $error = true;
       
    }
    else
    {
        if(!preg_match ("/^[a-zA-Z\s]+$/",$voorNaam))
        {
            $geenVoornaam = "Er mogen alleen letters ingetypt worden";
            $error = true;
        } 
    }
}
if(isset($_POST['familienaam']))
{
    $familieNaam = $_POST['familienaam'];
    if(empty($familieNaam))
    {
        $geenFamilienaam = "Er moet een familienaam ingevuld worden!";
        $error = true;
    }
    else
    {
        if(!preg_match ("/^[a-zA-Z\s]+$/",$familieNaam))
        {
            $geenFamilienaam = "Er mogen alleen letters ingetypt worden";
            $error = true;
        } 
    }
}
if(isset($_POST['rekeningnummer']))
{
    $account = $_POST['rekeningnummer'];
    if(empty($account)) 
    {
        $account = '';
        $accountErr = 'Geef een rekeningnummer op.';
        $error = true;
    }
    else
    {
            $reknrarray = explode('-', $account);
            $cleannumber = (int)ltrim($reknrarray[0] . $reknrarray[1] , '0');
            $check = fmod( $cleannumber, 97 );
            if ($check != $reknrarray[2])
            {
            $account = '';
            $accountErr = 'Rekeningnummer is ongeldig.';
            $error = true;
        }
    }
}
if($error == false)
{
    header("location: BevestigingReservatie.php?voornaam=$voorNaam&familienaam=$familieNaam");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inschrijven Concert</title>
</head>
<body>
<Titel>Bob Dylan</Titel>
<form method="post" action="Reservatie.php">
    <div>
    <label for="voornaam">Voornaam:</label>
    <input type="text" name="voornaam" placeholder="voornaam" id="voornaam"/>
    <span><?php echo $geenVoornaam;?></span>
    </div>
    <div>
    <label for="familienaam">Familienaam:</label>
    <input type="text" name="familienaam" placeholder="familienaam" id="voornaam"/>
    <span><?php echo $geenFamilienaam;?></span>
    </div>
    <div>
    <label for="plaats">Plaats:</label>
    <select name="plaats" id="plaats">
            <option value="0">Stage</option>
            <option value="1">Tribune</option>
            <option value="2">Balkon</option>
    </select>
    </div>
    <div>
    <label for="aantalpersonen">Aantal personen:</label>
    <input type="number" name="aantalpersonen" id="aantalpersonen" required/>
    </div>
    <div>
    <label for ="geboortedatum">Geboortedatum:</label>
    <input type="date" name="geboortedatum" id="geboortedatum"/>
    </div>
    <div>
    <label for ="rekeningnummer">Rekeningnummer:</label>
    <input type="text" name="rekeningnummer" placeholder="123-1234567-12" id="rekeningnummer"/>
    <span><?php echo  $accountErr;?></span>
    </div>
    <div>
     <label for ="email">Email:</label>
     <input type="email" name="email" placeholder="test@test.be" id="email" required/>
    </div>
    <button type="submit">Reserveren</button>
</form>
</body>
</html>