<?php
$persons = array
(
    array('Johanna', 'Den Doper', 117, 'Vrouw'),
    array('Mohamed', 'El Parisi', 40, 'Man')
);

$personsAssoc = array
(
    array('Firstname'=>'Johanna', 'Familienaam'=>'Den Doper','Leeftijd'=> 117, 'Geslacht'=>'Vrouw'),
    array('Firstname'=>'Mohamed', 'Familienaam'=>'El Parisi','Leeftijd'=> 40,'Geslacht'=>'Man')
);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met arrays in PHP</title>
</head>
<body>
    <table>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        <?php
            foreach ($persons as $person) { ?>
                <tr>
                    <td><?php echo $person{0}?></td>
                    <td><?php echo $person{1}?></td>
                    <td><?php echo $person{2}?></td>
                    <td><?php echo $person{3}?></td>
                </tr>
            <?php
            }
            ?>
    </table>
    <p>Naam</p>
    <table>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        <?php
            foreach ($personsAssoc as $person ) { ?>
                <tr>
                    <td><?php echo $person{'Firstname'}?></td>
                    <td><?php echo $person{'Familienaam'}?></td>
                    <td><?php echo $person{'Leeftijd'}?></td>
                    <td><?php echo $person{'Geslacht'}?></td>
                </tr>
            <?php
            }
            ?>
    </table>
</body>
</html>