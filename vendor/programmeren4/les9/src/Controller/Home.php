<?php
namespace Programmeren4\Les9\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        $model = new \Programmeren4\Les9\Model\Test();
        return $this->view('Home','Index',$model);
    }
}