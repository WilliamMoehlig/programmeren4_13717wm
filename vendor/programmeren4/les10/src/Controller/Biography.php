<?php
    namespace Programmeren4\Les10\Controller;
    
    class Biography extends \ModernWays\Mvc\Controller
    {
        public function show()
        {
            $model = new \Programmeren4\Les10\Model\Biography;
            return $this->view('Biography','Show',$model);
        }
    }
?>