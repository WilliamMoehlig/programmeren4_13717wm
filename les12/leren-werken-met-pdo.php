<?php
    try 
    {
         $pdo = new \PDO('mysql:host=localhost; dbname=williammoehlig;charset=utf8', 'williammoehlig', '');
         
         echo 'Connectie gemaakt';
         
         $command = $pdo->query("Select * from Article");
         
         //associatieve array kolommen en waarde per rij.
         $articles = $command->fetchAll(\PDO::FETCH_ASSOC);
         
         
         $command = $pdo->query("call ArticleSelectAll");
         
         $articlesOrdered = $command->fetchAll(\PDO::FETCH_ASSOC);
         
         
         $command = $pdo->query("call ArticleSelectOne(1)");
         
         $articlesOne = $command->fetchAll(\PDO::FETCH_ASSOC);
         
         $command = $pdo->exec("call ArticleInsert('Jaguar F-Pace', '2016-12-19', 203.56, @pId)");
         $command = $pdo->exec("call ArticleUpdate('BMW', '2016-12-19', 120.36, 4)");
         $command = $pdo->exec("call ArticleDelete(3)");
         
         $command = $pdo->query("call ArticleSelectAll");
         $articleSelect = $command->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\PDOException $e ) 
    {
        echo $e->getMessage();
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Leren werken met PDO</title>
</head>
<body>
    <pre>
        <?php var_dump($articles);?>
    </pre>
    <pre>
        //<?php var_dump($articlesOrdered);?>
    </pre>
    <pre>
        <?php var_dump($articlesOne);?>
    </pre>
    <pre>
        <?php var_dump($articleSelect);?>
    </pre>
</body>
</html>