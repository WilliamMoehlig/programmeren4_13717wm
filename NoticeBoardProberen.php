<<<<<<< HEAD
<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 10/12/2016
 * Time: 11:52
 */
    session_set_cookie_params(360);
    session_start();

    include ('vendor/modernways/dialog/src/Model/INotice.php');
    include ('vendor/modernways/dialog/src/Model/Notice.php');
    include ('vendor/modernways/dialog/src/Model/INoticeBoard.php');
    include ('vendor/modernways/dialog/src/Model/NoticeBoard.php');

    $errortitle = "";
    $errorcommentaar = "";
    $error = false;

    $nb = new ModernWays\Dialog\Model\NoticeBoard();

    if (isset($_POST['titel']))
    {
        $titel = $_POST['titel'];

        if (empty($titel)){
            $errortitle = "De titel mag niet leeg zijn";
            $error = true;
        }
    }
    else
    {
        if (isset($titel) && !preg_match("^[a-zA-Z0-9_]$",$titel))
        {
            $errortitle = "Er mogen alleen letters, cijfers en underscores gebruikt worden";
            $error = true;
        }
    }

    if (isset($_POST['commentaar']))
    {
        $commentaar = $_POST['commentaar'];

        if (empty($commentaar)){
            $errorcommentaar = "De commentaar mag niet leeg zijn";
            $error = true;
        }
    }
    else
    {
        if (isset($commentaar) && !preg_match("^[a-zA-Z0-9_]$",$commentaar))
        {
            $errorcommentaar = "Er mogen alleen letters, cijfers en underscores gebruikt worden";
            $error = true;
        }
    }

    if ($error == false && isset($commentaar) && isset($titel))
    {
        if (isset($_SESSION['models']))
        {
            $models = unserialize($_SESSION['models']);
        }

        $nb->startTimeInKey($titel);
        $nb->setTitle($titel);
        $nb->setText('testen van '.$titel);
        $nb->setCaption($commentaar);
        $nb->setCode('001');
        $nb->log();

        $models[] = $nb;
        $_SESSION['models'] = serialize($models);
    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proberen werken met NoticeBoard</title>
</head>
<body>
    <form method="post" action="NoticeBoardProberen.php">
        <span><?php echo $errortitle?></span></br>
        <label for="titel">Titel: </label>
        <input type="text" placeholder="Vul hier de titel in" name="titel" id="titel"/></br>
        <span><?php echo $errorcommentaar?></span></br>
        <label for="commentaar">Commentaar: </label>
        <input type="text" placeholder="Vul hier de commentaar in" name="commentaar" id="commentaar"/></br>
        <button type="submit">Toevoegen</button>
    </form>
    <div>
        <?php
            if (isset($_SESSION['models']))
            {
                $models = unserialize($_SESSION['models']);
                foreach($models as $value)
                {
                    $model = $value;
                    include('vendor/modernways/dialog/src/View/NoticeBoard.php');
                }
            }
        ?>
    </div>
</body>
</html>
=======
<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 10/12/2016
 * Time: 11:52
 */
    session_set_cookie_params(360);
    session_start();

    include ('vendor/modernways/dialog/src/Model/INotice.php');
    include ('vendor/modernways/dialog/src/Model/Notice.php');
    include ('vendor/modernways/dialog/src/Model/INoticeBoard.php');
    include ('vendor/modernways/dialog/src/Model/NoticeBoard.php');

    $errortitle = "";
    $errorcommentaar = "";
    $error = false;

    $nb = new ModernWays\Dialog\Model\NoticeBoard();

    if (isset($_POST['titel']))
    {
        $titel = $_POST['titel'];

        if (empty($titel)){
            $errortitle = "De titel mag niet leeg zijn";
            $error = true;
        }
    }
    else
    {
        if (!preg_match("^[a-zA-Z0-9_]$",$titel))
        {
            $errortitle = "Er mogen alleen letters, cijfers en underscores gebruikt worden";
            $error = true;
        }
    }

    if (isset($_POST['commentaar']))
    {
        $commentaar = $_POST['commentaar'];

        if (empty($commentaar)){
            $errorcommentaar = "De commentaar mag niet leeg zijn";
            $error = true;
        }
    }
    else
    {
        if (!preg_match("^[a-zA-Z0-9_]$",$commentaar))
        {
            $errorcommentaar = "Er mogen alleen letters, cijfers en underscores gebruikt worden";
            $error = true;
        }
    }

    if ($error == false)
    {
        if (isset($_SESSION['models']))
        {
            $models = unserialize($_SESSION['models']);
        }

        $nb->startTimeInKey($titel);
        $nb->setTitle($titel);
        $nb->setText('testen van '.$titel);
        $nb->setCaption($commentaar);
        $nb->setCode('001');
        $nb->log();

        $models[] = $nb;
        $_SESSION['models'] = serialize($models);
    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proberen werken met NoticeBoard</title>
</head>
<body>
    <form method="post" action="NoticeBoardProberen.php">
        <span><?php echo $errortitle?></span></br>
        <label for="titel">Titel: </label>
        <input type="text" placeholder="Vul hier de titel in" name="titel" id="titel"/></br>
        <span><?php echo $errorcommentaar?></span></br>
        <label for="commentaar">Commentaar: </label>
        <input type="text" placeholder="Vul hier de commentaar in" name="commentaar" id="commentaar"/></br>
        <button type="submit">Toevoegen</button>
    </form>
    <div>
        <?php
            if (isset($_SESSION['models']))
            {
                $models = unserialize($_SESSION['models']);
                foreach($models as $value)
                {
                    $model = $value;
                    include('vendor/modernways/dialog/src/View/NoticeBoard.php');
                }
            }
        ?>
    </div>
</body>
</html>
>>>>>>> 24697f728dffbd3d55e1cd1bebf05c12fa2416b5
